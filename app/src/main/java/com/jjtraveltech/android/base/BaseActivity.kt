package com.jjtraveltech.android.base

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import butterknife.OnClick
import com.jjtraveltech.android.util.customUIHelper.CustomProgressBarFragment

abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var customProgressBarFragment: CustomProgressBarFragment? = null

    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        ButterKnife.bind(this@BaseActivity)
        customProgressBarFragment = CustomProgressBarFragment(this@BaseActivity)
    }

    override fun showOfflineScreen() {
        hideContent()
    }

    override fun hideOfflineScreen() {
        showContent()
    }

    @OnClick(com.jjtraveltech.android.R.id.txt_retry, com.jjtraveltech.android.R.id.textView, com.jjtraveltech.android.R.id.textView2)
    fun onButtonClicked() {
        onRetryClickedForInternetCheck()
    }

    override fun getContext(): Context {
        return this@BaseActivity
    }

    protected abstract fun onRetryClickedForInternetCheck()

    protected abstract fun hideContent()

    protected abstract fun showContent()


}
