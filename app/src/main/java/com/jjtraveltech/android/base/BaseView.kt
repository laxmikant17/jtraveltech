package com.jjtraveltech.android.base

import android.content.Context

interface BaseView {

    fun getContext(): Context

    fun showOfflineScreen()

    fun hideOfflineScreen()
}
