package com.jjtraveltech.android.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjtraveltech.android.R;
import com.jjtraveltech.android.util.customUIHelper.CustomProgressBarFragment;
import com.jjtraveltech.android.util.customUIHelper.ICustomProgressBarView;

import butterknife.OnClick;

public abstract class BaseFragment extends Fragment implements BaseView,ICustomProgressBarView {

    CustomProgressBarFragment dialogFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_list, container, false);
        return view;
    }

    @Override
    public void showOfflineScreen() {
        hideContent();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void hideOfflineScreen() {
        showContent();
    }

    @OnClick({R.id.txt_retry, R.id.textView, R.id.textView2})
    public void onButtonClicked() {
        onRetryClickedForInternetCheck();
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    public abstract int getLayoutId();

    protected abstract void onRetryClickedForInternetCheck();

    protected abstract void hideContent();

    protected abstract void showContent();

    @Override
    public void showProgressBar() {

        dialogFragment = new CustomProgressBarFragment(getContext());
        dialogFragment.show();

    }

    @Override
    public void hideProgressBar() {
        if(dialogFragment!=null && dialogFragment.isShowing())
            dialogFragment.dismiss();
    }

}
