package com.jjtraveltech.android.util.helper

enum class ConnectionType {
    WIFI,
    EDGE,
    GPRS,
    THIRD_GENERATION,
    FOURTH_GENERATION,
    DEFAULT,
    NO_CONNECTION
}
