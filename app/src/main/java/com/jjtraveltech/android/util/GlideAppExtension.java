package com.jjtraveltech.android.util;

import android.annotation.SuppressLint;

import com.jjtraveltech.android.R;
import com.bumptech.glide.annotation.GlideExtension;
import com.bumptech.glide.annotation.GlideOption;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Laxmikant Mahamuni on 04-05-2018.
 */

@GlideExtension
public class GlideAppExtension {


    private GlideAppExtension() {
    }

    /**
     * Predefined configuration for normal image loading
     */
    @SuppressLint("CheckResult")
    @GlideOption
    public static void normalImage(RequestOptions options) {
        options
                .centerCrop()
                .error(R.color.error_image_back_color) // will be displayed if the image cannot be loaded;
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        /**
         *  ***Caching Strategy***
         *  DiskCacheStrategy.NONE - caches nothing
         *  DiskCacheStrategy.DATA - caches only the original full-resolution image
         *  DiskCacheStrategy.RESOURCE - caches only the final image, after reducing the resolution (and possibly transformations)
         *  DiskCacheStrategy.ALL - caches all versions of the image
         *  DiskCacheStrategy.AUTOMATIC - intelligently chooses a cache strategy based on the resource (default behavior of Glide 4.x)*/
    }

    /**
     * Predefined configuration for normal image loading
     */
    @SuppressLint("CheckResult")
    @GlideOption
    public static void defaultPrimaryImageInMyBookings(RequestOptions options) {
        options
                .centerCrop()
                .error(R.drawable.default_primary_image_mybookings_right_crop) // will be displayed if the image cannot be loaded;
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        /**
         *  ***Caching Strategy***
         *  DiskCacheStrategy.NONE - caches nothing
         *  DiskCacheStrategy.DATA - caches only the original full-resolution image
         *  DiskCacheStrategy.RESOURCE - caches only the final image, after reducing the resolution (and possibly transformations)
         *  DiskCacheStrategy.ALL - caches all versions of the image
         *  DiskCacheStrategy.AUTOMATIC - intelligently chooses a cache strategy based on the resource (default behavior of Glide 4.x)*/
    }

    /**
     * Predefined configuration for normal image loading
     */
    @SuppressLint("CheckResult")
    @GlideOption
    public static void defaultPrimaryImageBookingDetails(RequestOptions options) {
        options
                .centerCrop()
                .error(R.drawable.default_primary_image_mybooking) // will be displayed if the image cannot be loaded;
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        /**
         *  ***Caching Strategy***
         *  DiskCacheStrategy.NONE - caches nothing
         *  DiskCacheStrategy.DATA - caches only the original full-resolution image
         *  DiskCacheStrategy.RESOURCE - caches only the final image, after reducing the resolution (and possibly transformations)
         *  DiskCacheStrategy.ALL - caches all versions of the image
         *  DiskCacheStrategy.AUTOMATIC - intelligently chooses a cache strategy based on the resource (default behavior of Glide 4.x)*/
    }

    /**
     * Predefined configuration for gallery image loading
     */
    @SuppressLint("CheckResult")
    @GlideOption
    public static void fullImage(RequestOptions options) {
        options
                .centerCrop()
                .error(R.color.error_image_back_color) // will be displayed if the image cannot be loaded;
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        /**
         *  ***Caching Strategy***
         *  DiskCacheStrategy.NONE - caches nothing
         *  DiskCacheStrategy.DATA - caches only the original full-resolution image
         *  DiskCacheStrategy.RESOURCE - caches only the final image, after reducing the resolution (and possibly transformations)
         *  DiskCacheStrategy.ALL - caches all versions of the image
         *  DiskCacheStrategy.AUTOMATIC - intelligently chooses a cache strategy based on the resource (default behavior of Glide 4.x)*/
    }

    /**
     * Predefined configuration for thumbnail image loading
     */
    @SuppressLint("CheckResult")
    @GlideOption
    public static void thumbnailImage(RequestOptions options) {
        options
//                .placeholder(R.drawable.one)
                .error(R.drawable.error_image) // will be displayed if the image cannot be loaded;
                .centerCrop()
                .override(100, 100)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        /**
         *  ***Caching Strategy***
         *  DiskCacheStrategy.NONE - caches nothing
         *  DiskCacheStrategy.DATA - caches only the original full-resolution image
         *  DiskCacheStrategy.RESOURCE - caches only the final image, after reducing the resolution (and possibly transformations)
         *  DiskCacheStrategy.ALL - caches all versions of the image
         *  DiskCacheStrategy.AUTOMATIC - intelligently chooses a cache strategy based on the resource (default behavior of Glide 4.x)*/

    }
}
