package com.jjtraveltech.android.util.customUIHelper

interface ICustomProgressBarView {
    fun showProgressBar()

    fun hideProgressBar()
}
