package com.jjtraveltech.android.util.helper;

import android.content.Context;



public class Utils {

    private static boolean logEnabled = false;

    /**
     * Depends on internet speed, this class will decide how much records should be fetched at a time
     */
    public static String getBatchSize(Context context) {

        if (Connectivity.isOnline(context)) {
            boolean isFastInternetAvailable = Connectivity.isConnectedFast(context);
            return isFastInternetAvailable ? "100" : "20";
        } else {
            return null;
        }
    }

    public static String replaceSlashNfromAddress(String address) {
        String formattedAddress[];
        formattedAddress = address.split("\n");
        return formattedAddress[0];
    }

    public static void showSOP(String msg) {
        if (logEnabled) {
            String textMsg = ":::  --> " + msg;
            System.out.println(textMsg);
        }
    }

}
