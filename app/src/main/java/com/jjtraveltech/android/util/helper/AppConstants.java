package com.jjtraveltech.android.util.helper;

/**
 * Created by Laxmikant Mahamuni on 20-04-2018.
 */

public interface AppConstants {

    String ANDROID_CHANNEL_ID = "INTL-ANDROID:888888";

    String BASE_URL = "https://5e99a9b1bc561b0016af3540.mockapi.io/";

    String SHARED_PREFS_NAME = "APP_PREFERENCE";
    String PREFS_RESTAURANT_COUNTRY = "prefs_restaurant_country";
    String PREFS_PAGE_INDEX = "prefs_page";

    enum ENUM_COUNTRIES {DE, DEU}

    class PreferenceTags {
        public static final String PREFS_OFFLINE_SYNC_REQUIRED = "prefs_is_sync_required";
        public static final String PREFS_REALM_NAME = "prefs_database_name";
        public static final String PREFS_REALM_DIRECTORY_PATH = "prefs_directory_path";
        public static final String PREFS_DEFAULT_CITY_NAME = "prefs_default_city_name";
        public static final String PREFS_LAST_SEQUENCE = "prefs_last_sequence";
        public static final int SCHEMA_VERSION = 2;
        public static final String BAT_VERSION_CODE = "prefs_version_code";
    }

    class BundleExtras {
        public static final String EXTRA_MRAS_FULL_DATE = "extra_mras_full_date";
    }
}