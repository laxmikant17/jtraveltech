package com.jjtraveltech.android.util.helper;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import javax.inject.Inject;

import static android.net.ConnectivityManager.TYPE_MOBILE;
import static android.telephony.TelephonyManager.NETWORK_TYPE_1xRTT;
import static android.telephony.TelephonyManager.NETWORK_TYPE_CDMA;
import static android.telephony.TelephonyManager.NETWORK_TYPE_EDGE;
import static android.telephony.TelephonyManager.NETWORK_TYPE_EHRPD;
import static android.telephony.TelephonyManager.NETWORK_TYPE_EVDO_0;
import static android.telephony.TelephonyManager.NETWORK_TYPE_EVDO_A;
import static android.telephony.TelephonyManager.NETWORK_TYPE_EVDO_B;
import static android.telephony.TelephonyManager.NETWORK_TYPE_GPRS;
import static android.telephony.TelephonyManager.NETWORK_TYPE_HSDPA;
import static android.telephony.TelephonyManager.NETWORK_TYPE_HSPA;
import static android.telephony.TelephonyManager.NETWORK_TYPE_HSPAP;
import static android.telephony.TelephonyManager.NETWORK_TYPE_HSUPA;
import static android.telephony.TelephonyManager.NETWORK_TYPE_IDEN;
import static android.telephony.TelephonyManager.NETWORK_TYPE_LTE;
import static android.telephony.TelephonyManager.NETWORK_TYPE_UMTS;

public class Connectivity {

    @Inject
    public Connectivity(){

    }

    /**
     * Get the network info
     *
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        if (context != null)
        {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                return cm.getActiveNetworkInfo();
            }
        }
        return null;
    }


    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    public boolean isMobileConnected(Context context){
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == TYPE_MOBILE);
    }

    /**
     * Check if there is fast connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnectedFast(Context context) {
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        return (info != null && info.isConnected() && Connectivity.isConnectionFast(info.getType(), info.getSubtype()));
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public ConnectionType getConnectionSpeed(Context context){
        NetworkInfo info = Connectivity.getNetworkInfo(context);
        if(!isConnected(context)){
            return ConnectionType.NO_CONNECTION;
        }
        return getNetworkClass(info);
    }

    private static ConnectionType getNetworkClass(NetworkInfo networkInfo) {
        int networkType = networkInfo.getType();
        if (networkType == ConnectivityManager.TYPE_WIFI) {
            return ConnectionType.WIFI;
        }
        if(networkType != TYPE_MOBILE){
            return ConnectionType.DEFAULT;
        }
        networkType = networkInfo.getSubtype();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                return ConnectionType.EDGE;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
            case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
            case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:  //api<25 : replace by 17
                return ConnectionType.THIRD_GENERATION;
            case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
            case TelephonyManager.NETWORK_TYPE_IWLAN:  //api<25 : replace by 18
            case 19:  //LTE_CA
                return ConnectionType.FOURTH_GENERATION;
            default:
                return ConnectionType.DEFAULT;
        }
    }



    /**
     * Check if the connection is fast
     *
     * @param type
     * @param subType
     * @return
     */
    public static boolean isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return true;
        } else if (type == TYPE_MOBILE) {
            switch (subType) {
                case NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion
			 * to appropriate level to use these
			 */
                case NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

}