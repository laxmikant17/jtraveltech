package com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.adapter;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Objects;


public class ArticleOverview implements Parcelable, Comparable<ArticleOverview> {
    private String imageUrl;
    private String id;
    private String restaurantName;
    private String cuisineType;
    private String city;

    private String country;
    private Double distanceInKm;
    private Double latitude;
    private Double longitude;
    private String description;
    private String distanceInCharacters;
    private boolean shouldHideDistance;


    private ArticleOverview(Builder builder) {
        imageUrl = builder.imageUrl;
        id = builder.id;
        restaurantName = builder.restaurantName;
        cuisineType = builder.cuisineType;
        city = builder.city;
        country = builder.country;
        distanceInKm = builder.distanceInKm;
        latitude = builder.latitude;
        longitude = builder.longitude;
        description = builder.description;
        distanceInCharacters = builder.distanceInString;
        shouldHideDistance = builder.shouldHideDistance;
    }

    public ArticleOverview() {
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getId() {
        return id;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getCuisineType() {
        return cuisineType;
    }

    public String getCity() {
        return city;
    }

    public Double getDistanceInKm() {
        return distanceInKm;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getDistanceInCharacters() {
        return distanceInCharacters;
    }

    public boolean shouldShowDistance() {
        return shouldHideDistance;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageUrl);
        dest.writeString(this.id);
        dest.writeString(this.restaurantName);
        dest.writeString(this.cuisineType);
        dest.writeString(this.city);
        dest.writeString(this.country);
        dest.writeDouble(this.distanceInKm);
        dest.writeString(this.description);
        dest.writeString(this.distanceInCharacters);
        dest.writeByte((byte) (this.shouldHideDistance ? 1 : 0));
    }

    protected ArticleOverview(Parcel in) {
        this.imageUrl = in.readString();
        this.id = in.readString();
        this.restaurantName = in.readString();
        this.cuisineType = in.readString();
        this.city = in.readString();
        this.country = in.readString();
        this.distanceInKm = in.readDouble();
        this.description = in.readString();
        this.distanceInCharacters = in.readString();
        this.shouldHideDistance = in.readByte() != 0;
    }

    public static final Creator<ArticleOverview> CREATOR = new Creator<ArticleOverview>() {
        @Override
        public ArticleOverview createFromParcel(Parcel source) {
            return new ArticleOverview(source);
        }

        @Override
        public ArticleOverview[] newArray(int size) {
            return new ArticleOverview[size];
        }
    };

    @Override
    public int compareTo(@NonNull ArticleOverview o) {
        return Double.compare(distanceInKm, o.distanceInKm);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleOverview that = (ArticleOverview) o;
        return Objects.equals(imageUrl, that.imageUrl) &&
                Objects.equals(id, that.id) &&
                Objects.equals(restaurantName, that.restaurantName) &&
                Objects.equals(cuisineType, that.cuisineType) &&
                Objects.equals(city, that.city) &&
                Objects.equals(country, that.country) &&
                Objects.equals(distanceInKm, that.distanceInKm) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(description, that.description) &&
                Objects.equals(distanceInCharacters, that.distanceInCharacters);
    }

    @Override
    public int hashCode() {

        return Objects.hash(imageUrl, id, restaurantName, cuisineType, city, distanceInKm, latitude, longitude, description, distanceInCharacters);
    }


    /**
     * {@code ArticleOverview} builder static inner class.
     */
    public static final class Builder {
        private String imageUrl;
        private String id;
        private String restaurantName;
        private String cuisineType;
        private String city;
        private double distanceInKm;
        private Double latitude;
        private Double longitude;
        private String description;
        private String distanceInString;
        private boolean shouldHideDistance;
        private String country;

        public Builder() {
        }

        /**
         * Sets the {@code imageUrl} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code imageUrl} to set
         * @return a reference to this Builder
         */
        public Builder imageUrl(String val) {
            imageUrl = val;
            return this;
        }

        public Builder isShouldShowDistance(boolean shouldHideDistance) {
            this.shouldHideDistance = shouldHideDistance;
            return this;
        }

        /**
         * Sets the {@code imageUrl} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code imageUrl} to set
         * @return a reference to this Builder
         */
        public Builder distanceInString(String val) {
            distanceInString = val;
            return this;
        }

        /**
         * Sets the {@code id} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code id} to set
         * @return a reference to this Builder
         */
        public Builder id(String val) {
            id = val;
            return this;
        }

        /**
         * Sets the {@code restaurantName} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code restaurantName} to set
         * @return a reference to this Builder
         */
        public Builder restaurantName(String val) {
            restaurantName = val;
            return this;
        }

        /**
         * Sets the {@code cuisineType} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code cuisineType} to set
         * @return a reference to this Builder
         */
        public Builder cuisineType(String val) {
            cuisineType = val;
            return this;
        }

        /**
         * Sets the {@code city} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code city} to set
         * @return a reference to this Builder
         */
        public Builder city(String val) {
            city = val;
            return this;
        }

        /**
         * Sets the {@code country} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code country} to set
         * @return a reference to this Builder
         */
        public Builder country(String val) {
            country = val;
            return this;
        }

        /**
         * Sets the {@code distanceInKm} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code distanceInKm} to set
         * @return a reference to this Builder
         */
        public Builder distanceInKm(double val) {
            distanceInKm = val;
            return this;
        }

        /**
         * Sets the {@code latitude} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code latitude} to set
         * @return a reference to this Builder
         */
        public Builder latitude(Double val) {
            latitude = val;
            return this;
        }

        /**
         * Sets the {@code longitude} and returns a reference to this Builder so that the methods can be chained together.
         *
         * @param val the {@code longitude} to set
         * @return a reference to this Builder
         */
        public Builder longitude(Double val) {
            longitude = val;
            return this;
        }

        public Builder description(String val) {
            description = val;
            return this;
        }

        /**
         * Returns a {@code ArticleOverview} built from the parameters previously set.
         *
         * @return a {@code ArticleOverview} built with parameters of this {@code ArticleOverview.Builder}
         */
        public ArticleOverview build() {
            return new ArticleOverview(this);
        }

    }
}
