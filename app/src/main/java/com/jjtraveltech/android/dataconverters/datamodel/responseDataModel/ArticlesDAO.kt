package com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI

import android.content.Context

import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.RestAPI
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.endpoints.ChangesApiInterface

import java.util.ArrayList

import retrofit2.Response
import rx.Observable

class ArticlesDAO private constructor(private val mContext: Context) {


    fun getArticles(pageIndex: Int): Observable<Response<ArrayList<ArticlesResponse>>>
    {
        val articleApiInterface = RestAPI.initializeRetrofit(mContext)!!.create(ChangesApiInterface::class.java)
        return articleApiInterface.fetchChanges(pageIndex)
    }

    companion object {

        private var articlesDAO: ArticlesDAO? = null

        fun getInstance(context: Context): ArticlesDAO {

            if (articlesDAO == null) {
                articlesDAO = ArticlesDAO(context)
            }
            return articlesDAO as ArticlesDAO
        }
    }

}
