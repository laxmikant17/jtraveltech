package com.jjtraveltech.android.dataconverters.database;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public interface RealmCreator {

    Realm getRealm();

    void close();

    RealmConfiguration provideConfigurationIfAvailable();

    boolean deleteRestaurantDatabase();
}
