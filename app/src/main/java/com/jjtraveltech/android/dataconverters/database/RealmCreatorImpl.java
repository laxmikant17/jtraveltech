package com.jjtraveltech.android.dataconverters.database;

import android.content.Context;
import android.content.SharedPreferences;

import com.jjtraveltech.android.dagger.module.JTTDBModule;
import com.jjtraveltech.android.uimodules.articleList.extractDatabase.JTTRealmMigration;
import com.jjtraveltech.android.util.helper.AppConstants;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmConfiguration;
public class RealmCreatorImpl implements RealmCreator {

    private Context context;
    private RealmConfiguration realmConfiguration;
    private SharedPreferences sharedPreferences;
    private RealmConfiguration defaultConfig;

    public RealmCreatorImpl(Context context, RealmConfiguration realmConfiguration, SharedPreferences sharedPreferences) {
        this.context = context;
        this.realmConfiguration = realmConfiguration;
        this.sharedPreferences = sharedPreferences;
        defaultConfig = new RealmConfiguration.Builder().build();
    }

    // Constructor to created JTT User Database on JTTApplication screen
    public RealmCreatorImpl(Context context, boolean forBookingTable) {
        this.context = context;
    }

    @Override
    public synchronized Realm getRealm() {
        Realm.init(context);
        if (!isRealmFileUnzipped()) {
            realmConfiguration = provideConfigurationIfAvailable();
        }
        if (realmConfiguration == null) {
            return Realm.getDefaultInstance();
        }
        Realm.setDefaultConfiguration(realmConfiguration);
        return Realm.getDefaultInstance();
    }

    /**
     * Checks if current configuration is not default one
     *
     * @return boolean
     */
    private boolean isRealmFileUnzipped() {
        if (realmConfiguration == null) {
            realmConfiguration = provideConfigurationIfAvailable();
            return realmConfiguration.getMigration() != defaultConfig.getMigration();
        }
        return realmConfiguration.getMigration() != defaultConfig.getMigration();
    }

    @Override
    public synchronized void close() {
        Realm.getDefaultInstance().close();
    }

    /**
     * Checks if realm file has been unzipped and path is available in sharedPreference
     *
     * @return RealmConfig
     */
    @Override
    public RealmConfiguration provideConfigurationIfAvailable() {
        Realm.init(context);
        String parentPath = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, "");
        boolean isExist = new File(parentPath).exists();
        if (!isExist) {
            return null;
        }
        String dbName = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_NAME, "");
        return new RealmConfiguration.Builder().schemaVersion(AppConstants.PreferenceTags.SCHEMA_VERSION)
                .directory(new File(parentPath))
                .name(dbName)
                .addModule(new JTTDBModule())
                .migration(new JTTRealmMigration())
                .build();
    }

    @Override
    public boolean deleteRestaurantDatabase() {
        Realm.init(context);
        return Realm.deleteRealm(provideConfigurationIfAvailable());
    }

}
