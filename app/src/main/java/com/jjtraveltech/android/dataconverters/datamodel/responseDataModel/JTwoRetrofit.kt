package com.jjtraveltech.android.dataconverters.datamodel.responseDataModel

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jjtraveltech.android.util.helper.AppConstants

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Laxmikant Mahamuni on 26-03-2018.
 * Edited by Hardik on 04-07-2018
 */

/**
 * Initialize retrofit object for network calls
 */
object JTwoRetrofit {
    /**
     * provides retrofit instance for BAT apis
     * @return retrofit instance
     */
    val retrofit: Retrofit
        get() = Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(OkHttpClient.Builder().build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .build()

    private fun provideGson(): Gson {
        return GsonBuilder().setLenient().create()
    }

}
