package com.jjtraveltech.android.dataconverters.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result;

import java.util.ArrayList;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "JTTDB.db";
    public static final String ARTICLES_TABLE_NAME = "articles";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CREATED_AT = "createdAt";
    public static final String COLUMN_CONTENT = "content";
    public static final String COLUMN_COMMENTS = "comments";
    public static final String COLUMN_LIKES = "likes";
    public static final String COLUMN_IMAGE = "image";
    public static final String COLUMN_NAME = "name";
    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table " + ARTICLES_TABLE_NAME +
                        "(" + COLUMN_ID + " integer primary key, " + COLUMN_CREATED_AT + " text, " + COLUMN_CONTENT + " text, " +
                        COLUMN_COMMENTS + " text, " + COLUMN_LIKES + " text, " + COLUMN_IMAGE + " text, " + COLUMN_NAME + " text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }

    public boolean insertArticle(String id, String createdAt, String content, String comments, String likes, String image, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, id);
        contentValues.put(COLUMN_CREATED_AT, createdAt);
        contentValues.put(COLUMN_CONTENT, content);
        contentValues.put(COLUMN_COMMENTS, comments);
        contentValues.put(COLUMN_LIKES, likes);
        contentValues.put(COLUMN_IMAGE, image);
        contentValues.put(COLUMN_NAME, name);
        db.insert(ARTICLES_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from contacts where id=" + id + "", null);
        return res;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, ARTICLES_TABLE_NAME);
        return numRows;
    }

    public boolean updateArticle(String id, String createdAt, String content, String comments, String likes) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, id);
        contentValues.put(COLUMN_CREATED_AT, createdAt);
        contentValues.put(COLUMN_CONTENT, comments);
        contentValues.put(COLUMN_COMMENTS, comments);
        contentValues.put(COLUMN_LIKES, likes);
        db.update(ARTICLES_TABLE_NAME, contentValues, "id = ? ", new String[]{id});
        return true;
    }

    public Integer deleteArticles(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(ARTICLES_TABLE_NAME,
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public ArrayList<class_result> getAllArticleBatch(String batchNumber) {
        ArrayList<class_result> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from " + ARTICLES_TABLE_NAME + " where id >= " + batchNumber +" limit 10", null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                class_result obj = new class_result();
                obj.setId(cursor.getString(0));
                obj.setCreatedAt(cursor.getString(1));
                obj.setContent(cursor.getString(2));
                obj.setComments(cursor.getString(3));
                obj.setLikes(cursor.getString(4));
                obj.setImageURL(cursor.getString(5));
                obj.setName(cursor.getString(6));

                array_list.add(obj);
                cursor.moveToNext();
            }
        }

        return array_list;
    }
}