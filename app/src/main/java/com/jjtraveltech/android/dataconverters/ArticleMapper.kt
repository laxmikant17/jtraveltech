package com.jjtraveltech.android.dataconverters

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.adapter.ArticleOverview
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result_realm

import java.util.ArrayList

interface ArticleMapper {
    fun convertToRestaurantAdapter(restaurants: ArrayList<class_result_realm>, shouldShowDistance: Boolean): ArrayList<ArticleOverview>
}
