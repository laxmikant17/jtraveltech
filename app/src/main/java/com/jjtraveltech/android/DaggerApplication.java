package com.jjtraveltech.android;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.multidex.MultiDexApplication;

import com.jjtraveltech.android.dagger.component.AppComponent;
import com.jjtraveltech.android.dagger.component.ArticleComponent;
import com.jjtraveltech.android.dagger.component.ArticleListComponent;
import com.jjtraveltech.android.dagger.component.DaggerAppComponent;
import com.jjtraveltech.android.dagger.component.DaggerArticleComponent;
import com.jjtraveltech.android.dagger.component.DaggerArticleListComponent;
import com.jjtraveltech.android.dagger.component.DaggerSplashComponent;
import com.jjtraveltech.android.dagger.component.SplashComponent;
import com.jjtraveltech.android.dagger.module.AppModule;
import com.jjtraveltech.android.dagger.module.ArticleModule;
import com.jjtraveltech.android.dagger.module.SplashModule;
import com.jjtraveltech.android.util.helper.SharedPreferenceHelper;

public abstract class DaggerApplication extends MultiDexApplication {

    private static AppComponent appComponent;
    private SharedPreferences sharedPreferences;


    @Override
    public void onCreate() {
        super.onCreate();
        getAppComponent().inject(this);
        sharedPreferences = SharedPreferenceHelper.getInstance().getSharedPreferences(this);
//        ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifeCycleObserver());
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.
                    builder().
                    appModule(new AppModule(this)).
                    build();
        }
        return appComponent;
    }

    /**
     * Accepts activity instance for activity module
     * we are not maintaining single instance of activityComponent as previous instance
     * of activity bounded to activityComponent would have been destroyed,
     * so it is obvious to pass alive instance of activity
     *
     * @param activity activity instance
     * @return activity component
     */
    public SplashComponent getSplashComponent(Activity activity) {
        return DaggerSplashComponent.
                builder().
                splashModule(new SplashModule(activity)).
                appComponent(getAppComponent()).
                build();
    }

    public ArticleListComponent getRestaurantListComponent() {
        return DaggerArticleListComponent.
                builder().
                appComponent(getAppComponent()).
                build();
    }

    /**
     * provides Restaurant component containing location and restaurant module
     *
     * @param articleModule restaurant module
     * @return Restaurant component instance
     */
    public ArticleComponent getRestaurantComponent(ArticleModule articleModule) {
        return DaggerArticleComponent.
                builder().
                appComponent(getAppComponent()).
                articleModule(articleModule).build();
    }
}
