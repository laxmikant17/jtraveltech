package com.jjtraveltech.android.dagger.module;


import android.support.v4.app.Fragment;

import com.jjtraveltech.android.dataconverters.ArticleMapper;
import com.jjtraveltech.android.dataconverters.ArticleMapperImpl;
import com.jjtraveltech.android.uimodules.articleList.view.ArticleListView;
import com.jjtraveltech.android.util.helper.Connectivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Locale;

import dagger.Module;
import dagger.Provides;

@Module
public class ArticleModule {
    private final Fragment fragment;
    private ArticleListView view;

    public ArticleModule(Fragment fragment) {
        this.fragment = fragment;
        if (this.fragment instanceof ArticleListView) {
            view = (ArticleListView) fragment;
        }
    }

    @Provides
    public Fragment provideActivity() {
        return fragment;
    }

    @Provides
    public FusedLocationProviderClient provideFusedLocationProviderClient(Fragment activity) {
        return LocationServices.getFusedLocationProviderClient(activity.getActivity());
    }


    @Provides
    public LocationRequest provideLocationRequest() {
        // Create the location request to start receiving updates
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); //need user's accurate location
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setNumUpdates(1);
        return mLocationRequest;
    }

    @Provides
    public ArticleListView provideView() {
        return view;
    }

    @Provides
    public ArticleMapper provideRestaurantMapper(Fragment activity, Locale locale) {
        return new ArticleMapperImpl(activity.getContext(), locale);
    }

    @Provides
    public Locale provideCurrentLocale() {
        return Locale.getDefault();
    }

    @Provides
    public Connectivity provideConnectivity() {
        return new Connectivity();
    }

}
