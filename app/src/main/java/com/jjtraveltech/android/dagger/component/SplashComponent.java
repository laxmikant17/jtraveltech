package com.jjtraveltech.android.dagger.component;

import com.jjtraveltech.android.dagger.module.SplashModule;
import com.jjtraveltech.android.dagger.scope.CustomScope;
import com.jjtraveltech.android.uimodules.splash.SplashActivity;

import dagger.Component;

@CustomScope
@Component(dependencies = AppComponent.class, modules = SplashModule.class)
public interface SplashComponent {

    void inject(SplashActivity splashActivity);
}
