package com.jjtraveltech.android.dagger.scope

import javax.inject.Qualifier

@MustBeDocumented
@Qualifier
annotation class JTTAPI
