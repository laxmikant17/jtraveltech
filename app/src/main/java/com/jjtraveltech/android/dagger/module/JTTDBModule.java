package com.jjtraveltech.android.dagger.module;

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result_realm;

import io.realm.annotations.RealmModule;

@RealmModule(classes = {class_result_realm.class})
public class JTTDBModule {
}