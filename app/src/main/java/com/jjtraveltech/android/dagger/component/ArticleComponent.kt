package com.jjtraveltech.android.dagger.component

import com.jjtraveltech.android.dagger.module.ArticleModule
import com.jjtraveltech.android.dagger.scope.CustomScope
import com.jjtraveltech.android.uimodules.articleList.view.ArticlesListFragment

import dagger.Component

@CustomScope
@Component(dependencies = [AppComponent::class], modules = [ArticleModule::class])
interface ArticleComponent {


    fun inject(articlesListFragment: ArticlesListFragment)
}
