package com.jjtraveltech.android.dagger.component

import android.content.Context
import android.content.SharedPreferences

import com.jjtraveltech.android.DaggerApplication
import com.jjtraveltech.android.dagger.module.AppModule
import com.jjtraveltech.android.dagger.scope.JTTAPI
import com.jjtraveltech.android.dataconverters.database.RealmCreator
import javax.inject.Singleton
import dagger.Component
import retrofit2.Retrofit

/**
 * Created by Laxmikant Mahamuni on 22-03-2018.
 */

/**
 * This interface specifies application components,
 * that which can be injectable components and those who can consume these injections
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    val context: Context

    val realmCreator: RealmCreator

    @JTTAPI
    fun retrofitForBAT(): Retrofit

    fun sharedPreferences(): SharedPreferences

    fun inject(daggerApplication: DaggerApplication)
}
