package com.jjtraveltech.android.dagger.component

import com.jjtraveltech.android.dagger.scope.CustomScope
import com.jjtraveltech.android.uimodules.articleList.adapters.BaseListAdapter

import dagger.Component

@CustomScope
@Component(dependencies = [AppComponent::class])
interface ArticleListComponent {
    fun inject(baseListAdapter: BaseListAdapter)
}
