package com.jjtraveltech.android.dagger.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.jjtraveltech.android.dagger.scope.JTTAPI
import com.jjtraveltech.android.dataconverters.database.RealmCreator
import com.jjtraveltech.android.dataconverters.database.RealmCreatorImpl
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.JTwoRetrofit
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.endpoints.ChangesApiInterface
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalDataRepository
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository
import com.jjtraveltech.android.util.helper.AppConstants
import com.jjtraveltech.android.util.helper.AppConstants.SHARED_PREFS_NAME
import com.jjtraveltech.android.util.helper.Connectivity
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import retrofit2.Retrofit
import java.io.File
import javax.inject.Singleton

/**
 * Created by Laxmikant Mahamuni on 22-03-2018.
 */


/**
 * Module annotation specifies this is an injectable class
 */
@Module
class AppModule(private val mApplication: Application) {

    /**
     * Provides application context throughout the application life cycle
     */
    @Provides
    @Singleton
    fun provideContext(): Context {
        return mApplication
    }

    /**
     * Initialize retrofit object and provide it to application
     */
    @Singleton
    @JTTAPI
    @Provides
    fun provideRetrofit(): Retrofit {
        return JTwoRetrofit.retrofit
    }

    @Provides
    @Singleton
    fun provideApiInterface(@JTTAPI retrofit: Retrofit): ChangesApiInterface {
        return retrofit.create(ChangesApiInterface::class.java)
    }


    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideRealmConfiguration(context: Context, sharedPreferences: SharedPreferences): RealmConfiguration {
        Realm.init(context)
        val parentPath = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_DIRECTORY_PATH, "")
        val isExist = File(parentPath!!).exists()
        if (!isExist) {
            return RealmConfiguration.Builder().build()
        }
        val dbName = sharedPreferences.getString(AppConstants.PreferenceTags.PREFS_REALM_NAME, "")
        return RealmConfiguration.Builder()
                .schemaVersion(AppConstants.PreferenceTags.SCHEMA_VERSION.toLong())
                .directory(File(parentPath))
                .name(dbName!!)
                .build()
    }

    @Provides
    @Singleton
    fun provideRealmCreator(context: Context, realmConfiguration: RealmConfiguration, sharedPreferences: SharedPreferences): RealmCreator {
        return RealmCreatorImpl(context, realmConfiguration, sharedPreferences)
    }

    /**
     * Provides LocalRepo instance to perform database interaction
     *
     * @param realmCreator realmDB Instance required to perform db operation
     * @return LocalRepo instance
     */
    @Provides
    @Singleton
    fun provideLocalRepository(realmCreator: RealmCreator): LocalDataRepository {
        return LocalRepository(realmCreator)
    }


    @Provides
    @Singleton
    fun provideConnectivity(): Connectivity {
        return Connectivity()
    }

}
