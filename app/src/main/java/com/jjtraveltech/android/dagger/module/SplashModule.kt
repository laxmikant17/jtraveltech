package com.jjtraveltech.android.dagger.module

import android.app.Activity

import com.jjtraveltech.android.uimodules.splash.SplashActions
import com.jjtraveltech.android.uimodules.splash.SplashPresenter

import dagger.Module
import dagger.Provides

@Module
class SplashModule(internal var activity: Activity) {

    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun provideSplashPresenterImpl(splashPresenter: SplashPresenter): SplashActions {
        return splashPresenter
    }

}
