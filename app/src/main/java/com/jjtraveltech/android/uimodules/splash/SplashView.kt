package com.jjtraveltech.android.uimodules.splash


interface SplashView {

    fun showSpinner()

    fun hideSpinner()

    fun launchMainActivity()

}
