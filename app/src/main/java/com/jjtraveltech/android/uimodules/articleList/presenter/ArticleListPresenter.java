package com.jjtraveltech.android.uimodules.articleList.presenter;

import android.content.SharedPreferences;

import com.jjtraveltech.android.dataconverters.ArticleMapper;
import com.jjtraveltech.android.uimodules.articleList.view.ArticleListView;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository;
import com.jjtraveltech.android.util.helper.Connectivity;

import javax.inject.Inject;


public class ArticleListPresenter extends BaseListPresenter {

    public ArticleListView view;

    @Inject
    public ArticleListPresenter(ArticleListView view, SharedPreferences sharedPreferences, LocalRepository localRepository, ArticleMapper articleMapper, Connectivity connectivity) {
        super(view, sharedPreferences, localRepository, articleMapper, connectivity);
        this.view = view;
    }
}