package com.jjtraveltech.android.uimodules.mainScreen.interfaces

import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result_realm
import java.util.*


interface LocalDataRepository {

//    fun addOrUpdateArticleList(articleList: ArrayList<class_result_realm>, realmCreator: RealmCreator)
    fun addOrUpdateArticleList(articleList: ArrayList<class_result_realm>)

}
