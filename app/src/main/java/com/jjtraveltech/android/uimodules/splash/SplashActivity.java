package com.jjtraveltech.android.uimodules.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.jjtraveltech.android.JTTApplication;
import com.jjtraveltech.android.R;
import com.jjtraveltech.android.uimodules.mainScreen.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;



public class SplashActivity extends Activity implements SplashView {


    @Inject
    SplashActions splashActions;

    @Inject
    Activity activity;

    @BindView(R.id.progress)
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        JTTApplication.getInstance().getSplashComponent(this).inject(this);
        splashActions.onAttach(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void showSpinner() {
        if (progress != null) {
            progress.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void hideSpinner() {
        if (progress != null) {
            progress.setVisibility(View.GONE);
        }
    }

    /**
     * Launch Main activity once the splash timer is completed
     */
    @Override
    public void launchMainActivity() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

}
