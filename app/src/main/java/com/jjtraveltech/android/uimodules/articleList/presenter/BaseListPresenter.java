package com.jjtraveltech.android.uimodules.articleList.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.jjtraveltech.android.dataconverters.ArticleMapper;
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.ArticlesDAO;
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse;
import com.jjtraveltech.android.dataconverters.db.DBHelper;
import com.jjtraveltech.android.uimodules.articleList.view.ArticleListView;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalDataRepository;
import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository;
import com.jjtraveltech.android.util.helper.AppConstants;
import com.jjtraveltech.android.util.helper.Connectivity;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;
import rx.Subscriber;


public class BaseListPresenter implements ArticleListActions {

    private final LocalDataRepository localRepository;
    private SharedPreferences sharedPreferences;
    private ArticleListView view;

    private CompositeDisposable compositeDisposable;
    public ArrayList<ArticlesResponse> articlesResponses;

    private int pageIndexDB = 0;

    public boolean isFistCall = true;


    @Inject
    public BaseListPresenter(ArticleListView view, SharedPreferences sharedPreferences, LocalRepository localRepository, ArticleMapper articleMapper, Connectivity connectivity) {
        this.view = view;
        this.sharedPreferences = sharedPreferences;
        this.localRepository = localRepository;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onViewCreated() {
        view.hideOfflineScreen();
        view.showSpinner();

        mydb = new DBHelper(view.getContext());
        pageIndexDB = sharedPreferences.getInt(AppConstants.PREFS_PAGE_INDEX, 0);

        if (pageIndexDB == 0)
            fetchArticlesFromWeb(view.getContext(), pageIndexDB + 1);
        else
            fetchArticlesFromDB(pageIndexDB);
    }

    public void fetchArticlesFromWeb(Context context, int pageIndex) {

        ArticlesDAO.Companion.getInstance(context)
                .getArticles(pageIndex)
                .subscribeOn(rx.schedulers.Schedulers.io())
                .observeOn(rx.android.schedulers.AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ArrayList<ArticlesResponse>>>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Response<ArrayList<ArticlesResponse>> articlesResponse) {
                        if (articlesResponse != null && articlesResponse.body() != null
                                && articlesResponse.body().size() > 0) {
                            articlesResponses = articlesResponse.body();
                            mapDataToTable(articlesResponses, context, pageIndex);
                        }
                    }
                });
    }

    /**
     * Remove any observable we were listening to
     */
    @Override
    public void onViewDetach() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

    /**
     * insert/update/delete data in database
     */
    private DBHelper mydb;

    private void mapDataToTable(ArrayList<ArticlesResponse> response, Context context, int pageIndex) {
        try {

            for (ArticlesResponse articlesResponse : response) {

                String image = null;
                if (articlesResponse.getMedia() != null &&
                        articlesResponse.getMedia().size() > 0 &&
                        articlesResponse.getMedia().get(0) != null &&
                        articlesResponse.getMedia().get(0).getImage() != null)
                    image = articlesResponse.getMedia().get(0).getImage();

                if (mydb.insertArticle(articlesResponse.getId(), articlesResponse.getCreatedAt(),
                        articlesResponse.getContent(), articlesResponse.getComments(),
                        articlesResponse.getLikes(), image, articlesResponse.getUser().get(0).getName())) {
                }
            }
            Toast.makeText(context, "done",
                    Toast.LENGTH_SHORT).show();
            sharedPreferences.edit().putInt(AppConstants.PREFS_PAGE_INDEX, pageIndex).apply();
            fetchArticlesFromDB(pageIndex);
            Log.d("DB_Log", "Insertion completed!!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fetchArticlesFromDB(int pageIndex) {
        try {
            String batchNumber = "" + ((pageIndex * 10) - 9);

            ArrayList<class_result> array_list = mydb.getAllArticleBatch(batchNumber);
            showDataOnUI(array_list, pageIndex);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDataOnUI(ArrayList<class_result> articleOverviews, int pageIndex) {
        if (articleOverviews.size() > 0)
            if (pageIndex == 1) {
                view.hideSpinner();
                view.showData(articleOverviews);
                isFistCall = false;
            } else {
                view.updateData(articleOverviews);
            }
    }

}