package com.jjtraveltech.android.uimodules.splash

import dagger.Provides


interface SplashActions {

    fun onAttach(splashView: SplashView)
}
