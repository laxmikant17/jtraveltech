package com.jjtraveltech.android.uimodules.articleList.presenter

interface ArticleListActions {
    fun onViewCreated()

    fun onViewDetach()
}
