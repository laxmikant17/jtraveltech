package com.jjtraveltech.android.uimodules.articleList.extractDatabase;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class Migration implements RealmMigration {

    private final String TAG = Migration.class.getSimpleName();
    private String hashValue = "Bookatable";

    @Override
    public void migrate(final DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();
        Log.i(TAG, "Old Version: " + oldVersion + " NewVersion: " + newVersion);
        if (oldVersion == 1) {
            RealmObjectSchema appSettingForceUpdateInfoSchema = schema.create("AppSettingForceUpdateInfo")
                    .addField("url", String.class, FieldAttribute.PRIMARY_KEY)
                    .setRequired("url", true)
                    .addRealmListField("versions", String.class)
                    .addField("languageCode", String.class)
                    .addField("title", String.class)
                    .addField("body", String.class)
                    .addField("buttonTitle", String.class);

            RealmObjectSchema quoteSchema = schema.create("AppSettings")
                    .addField("id", String.class, FieldAttribute.PRIMARY_KEY)
                    .setRequired("id", true)
                    .addField("vendor", String.class)
                    .addField("forceAppUpdate", Boolean.class)
                    .setRequired("forceAppUpdate", true)
                    .addRealmObjectField("appSettingForceUpdateInfo", appSettingForceUpdateInfoSchema);


            oldVersion++;
        }
    }

    @Override
    public int hashCode() {
        return hashValue.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Migration);
    }

}
