package com.jjtraveltech.android.uimodules.articleList.view

import com.jjtraveltech.android.base.BaseView
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result
import com.jjtraveltech.android.dataconverters.datamodel.responseDataModel.articlesAPI.response.ArticlesResponse
import java.util.*

interface ArticleListView : BaseView {

    fun showSpinner()

    fun hideSpinner()

    fun showData(articlesResponseArrayList: ArrayList<class_result>)

    fun updateData(articlesResponseArrayList: ArrayList<class_result>)

    fun hideErrorView()

}