package com.jjtraveltech.android.uimodules.splash;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;

import com.jjtraveltech.android.uimodules.mainScreen.interfaces.LocalRepository;
import com.jjtraveltech.android.uimodules.articleList.extractDatabase.UnzipDB;
import com.jjtraveltech.android.util.helper.Connectivity;

import javax.inject.Inject;

public class SplashPresenter implements SplashActions {

    private final int SPLASH_TIME = 1000;

    @Inject
    Context context;

    /**
     * injecting activity and sharedPreference instance
     *
     * @param activity activity instance
     */
    @Inject
    public SplashPresenter(Activity activity) {
    }

    private SplashView splashView;

    @Override
    public void onAttach(SplashView splashView) {
        this.splashView = splashView;

        getSplashView().showSpinner();
        android.os.Handler handler = new android.os.Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            if (getSplashView() == null)
                return;
            getSplashView().hideSpinner();

            getSplashView().launchMainActivity();
        }, SPLASH_TIME);
    }

    private SplashView getSplashView() {
        return splashView;
    }

}
