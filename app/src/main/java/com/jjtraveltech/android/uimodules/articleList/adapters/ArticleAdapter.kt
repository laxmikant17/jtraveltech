package com.jjtraveltech.android.uimodules.articleList.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jjtraveltech.android.dataconverters.datamodel.realmDataModel.realmDatabaseModel.class_result
import java.util.*

class ArticleAdapter(private val activity: Activity) : BaseListAdapter(activity) {
    private var viewHolder: RestaurantViewHolder? = null
    private var overviewArrayList: ArrayList<class_result>? = null

    override fun getRestaurantViewHolder(): RestaurantViewHolder? {
        return viewHolder
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val view = LayoutInflater.from(activity).inflate(com.jjtraveltech.android.R.layout.adapter_article_item, parent, false)
        viewHolder = RestaurantViewHolder(view)
        return viewHolder as RestaurantViewHolder
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        this.overviewArrayList = listData
        if (overviewArrayList != null && overviewArrayList!!.size != 0) {
            val articleOverview = overviewArrayList!![position]
            bindDataToViews(articleOverview, holder, position)
        }
    }

    private fun bindDataToViews(articleOverview: class_result?, holder: RestaurantViewHolder, position: Int) {
        if (articleOverview != null && articleOverview.name != null) {

            holder.txtRestaurantName.text = articleOverview.name

            if (articleOverview.imageURL != null) {
                loadImage(articleOverview.imageURL, holder.imgRestaurant, holder.imgRestaurantError, holder.textViewImageError)
            }
        }
    }

}
